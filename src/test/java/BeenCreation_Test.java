import app.App;
import app.spring.config.YamlConfig;
import app.spring.config.YamlResponses;
import bot.Launcher;
import bot.vk.*;
import com.vk.api.sdk.client.VkApiClient;
import com.vk.api.sdk.client.actors.GroupActor;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.BeanFactoryAnnotationUtils;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationContext;

public class BeenCreation_Test {
    public static ApplicationContext ctx = SpringApplication.run(App.class);

    @Test
    void app() {
        ctx.getBean(VkApiClient.class);
    }

    @Test
    void config() {
        YamlConfig appConfig = ctx.getBean(YamlConfig.class);
        Assertions.assertNotNull(appConfig.getNsfwBot(), "Config for nsfwBot not loaded!");
        Assertions.assertNotNull(appConfig.getMemeBot(), "Config for memeBot not loaded!");
    }

    @Test
    void responses() {
        YamlResponses botResponses = ctx.getBean(YamlResponses.class);
        Assertions.assertNotNull(botResponses.getNsfwResponses(), "Responses for nsfwBot not loaded!");
        Assertions.assertNotNull(botResponses.getMemeResponses(), "Responses for memeBot not loaded!");
    }

    @Test
    void vk() {
        for(String bean : ctx.getBeanDefinitionNames()) {
            System.out.println(bean);
        }
        AutowireCapableBeanFactory beanFactory = ctx.getAutowireCapableBeanFactory();
        String[] contentTypes = {"nsfw", "meme"};

        for(String qualifier : contentTypes) {
            BeanFactoryAnnotationUtils.qualifiedBeanOfType(beanFactory, Launcher.class, qualifier);
            BeanFactoryAnnotationUtils.qualifiedBeanOfType(beanFactory, VkBot.class, qualifier);
            BeanFactoryAnnotationUtils.qualifiedBeanOfType(beanFactory, GroupActor.class, qualifier);
            BeanFactoryAnnotationUtils.qualifiedBeanOfType(beanFactory, ApiEventHandler.class, qualifier);
            BeanFactoryAnnotationUtils.qualifiedBeanOfType(beanFactory, MessageHandler.class, qualifier);
            BeanFactoryAnnotationUtils.qualifiedBeanOfType(beanFactory, ResponseMaker.class, qualifier);
            BeanFactoryAnnotationUtils.qualifiedBeanOfType(beanFactory, ImageSaver.class, qualifier);
        }
    }
}
