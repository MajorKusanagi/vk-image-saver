package bot.vk;

import app.spring.config.YamlResponses;
import com.vk.api.sdk.client.VkApiClient;
import com.vk.api.sdk.client.actors.GroupActor;
import com.vk.api.sdk.exceptions.ApiException;
import com.vk.api.sdk.exceptions.ClientException;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Random;

public class ResponseMaker
{
	private static final Random random = new Random();
	@Autowired
	private VkApiClient vkClient;
	private GroupActor groupActor;
	private YamlResponses.BotResponses responses;

	public ResponseMaker(GroupActor groupActor, YamlResponses.BotResponses responses) {
		this.groupActor = groupActor;
		this.responses = responses;
	}

	public void botCatch(int toId) {
		try {
			List<String> catchResponses = responses.getCatched();
			String selectedResponse = catchResponses.get(random.nextInt(catchResponses.size()));
			vkClient.messages().send(groupActor).message(selectedResponse).userId(toId).randomId(random.nextInt()).execute();
		}
		catch(ApiException|ClientException e) {
			e.printStackTrace();
		}
	}

	public void botPong(int toId) {
		try {
			List<String> pongResponses = responses.getPong();
			String selectedResponse = pongResponses.get(random.nextInt(pongResponses.size()));
			vkClient.messages().send(groupActor).message(selectedResponse).userId(toId).randomId(random.nextInt()).execute();
		}
		catch(ApiException|ClientException e) {
			e.printStackTrace();
		}
	}

	public void botNoImage(int toId) {
		try {
			List<String> noImageResponses = responses.getNoImage();
			String selectedResponse = noImageResponses.get(random.nextInt(noImageResponses.size()));
			vkClient.messages().send(groupActor).message(selectedResponse).userId(toId).randomId(random.nextInt()).execute();
		}
		catch(ApiException|ClientException e) {
			e.printStackTrace();
		}
	}

	public void error(int toId, ImageSaverStatus error) {
		try {
			List<String> errorResponses = responses.getError();
			String selectedResponse = errorResponses.get(random.nextInt(errorResponses.size()));
			vkClient.messages().send(groupActor).message(selectedResponse+" ("+error.getValue()+")").userId(toId).randomId(random.nextInt()).execute();
		}
		catch(ApiException|ClientException e) {
			e.printStackTrace();
		}
	}
}
