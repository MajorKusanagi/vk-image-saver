package bot.vk;

import app.App;
import app.LogController;
import com.vk.api.sdk.client.VkApiClient;
import com.vk.api.sdk.client.actors.GroupActor;
import org.apache.logging.log4j.Level;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.BeanFactoryAnnotationUtils;

public class VkBot extends Thread {
    @Autowired
    private VkApiClient vkClient;
    private GroupActor groupActor;
    private String qualifier;

    public VkBot(GroupActor groupActor, String qualifier) {
        this.groupActor = groupActor;
        this.qualifier = qualifier;
    }

    @Override
    public void run() {
        try {
            if (!vkClient.groups().getLongPollSettings(groupActor, groupActor.getGroupId()).execute().getIsEnabled()) {
                vkClient.groups().setLongPollSettings(groupActor, groupActor.getGroupId()).enabled(true).wallPostNew(true).execute();
            }

            ApiEventHandler handler = BeanFactoryAnnotationUtils.qualifiedBeanOfType(App.ctx.getAutowireCapableBeanFactory(), ApiEventHandler.class, qualifier);
            LogController.log(groupActor.getGroupId(), Level.INFO, "connected");
            handler.run();
        }
        catch(Exception e) {
            LogController.log(groupActor.getGroupId(), Level.ERROR, "exception");
            e.printStackTrace();
        }
    }
}
