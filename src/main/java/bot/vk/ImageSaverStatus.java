package bot.vk;

import com.google.gson.annotations.SerializedName;

public enum ImageSaverStatus {
	@SerializedName("ok")
	OK("ok"),
	@SerializedName("vkError")
	VK_ERROR("vkError"),
	@SerializedName("pyError")
	PY_ERROR("pyError"),
	@SerializedName("javaError")
	JAVA_ERROR("javaError");

	private final String value;

	ImageSaverStatus(String value) {
		this.value = value;
	}

	public String getValue() {
		return this.value;
	}

	public String toString() {
		return this.value.toLowerCase();
	}
}
