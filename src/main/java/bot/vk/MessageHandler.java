package bot.vk;

import com.vk.api.sdk.objects.messages.Message;
import com.vk.api.sdk.objects.messages.MessageAttachmentType;

public class MessageHandler {
	ImageSaver imageSaver;
	ResponseMaker responseMaker;

	public MessageHandler(ImageSaver imageSaver, ResponseMaker responseMaker) {
		this.imageSaver = imageSaver;
		this.responseMaker = responseMaker;
	}

	public void read(Message message) {
		if(!message.getAttachments().isEmpty() && message.getAttachments().get(0).getType() == MessageAttachmentType.PHOTO) {
			ImageSaverStatus result = imageSaver.saveImage(message);
			if(result == ImageSaverStatus.OK) {
				responseMaker.botCatch(message.getFromId());
			}
			else {
				responseMaker.error(message.getFromId(), result);
			}
		}
		else {
			if(message.getText().toLowerCase().contains("ping")) {
				responseMaker.botPong(message.getFromId());
			}
			else {
				responseMaker.botNoImage(message.getFromId());
			}
		}
	}
}
