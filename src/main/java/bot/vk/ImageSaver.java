package bot.vk;

import com.vk.api.sdk.objects.photos.PhotoSizes;
import com.vk.api.sdk.objects.messages.Message;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Comparator;

public class ImageSaver
{
	private String path;

	public ImageSaver(String path) {
		this.path = path;
	}

	public ImageSaverStatus saveImage(Message message) {
		URL link = message.getAttachments().get(0).getPhoto().getSizes().stream().max(Comparator.comparingInt(PhotoSizes::getHeight)).orElseThrow(RuntimeException::new).getUrl();

		BufferedImage image;
		try {
			image = ImageIO.read(link);
		}
		catch(IOException e) {
			e.printStackTrace();
			return ImageSaverStatus.VK_ERROR;
		}

		try {
			ImageIO.write(image, "jpg", new File(path + System.currentTimeMillis() + ".jpg"));
		}
		catch(IOException e) {
			e.printStackTrace();
			return ImageSaverStatus.JAVA_ERROR;
		}

		return ImageSaverStatus.OK;
	}
}
