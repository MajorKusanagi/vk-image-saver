package bot;

import app.App;
import org.springframework.beans.factory.annotation.BeanFactoryAnnotationUtils;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Launcher {
    private static ScheduledExecutorService exec = Executors.newSingleThreadScheduledExecutor();
    Thread bot;
    String qualifier;

    public Launcher(Thread bot) {
        this.bot = bot;
    }

    public Launcher(Thread bot, String qualifier) {
        this.bot = bot;
        this.qualifier = qualifier;
    }

    public void launch() {
        bot.start();

        exec.scheduleAtFixedRate(() -> {
            if(!bot.isAlive()) {
                if(qualifier != null)
                    bot = BeanFactoryAnnotationUtils.qualifiedBeanOfType(App.ctx.getAutowireCapableBeanFactory(), bot.getClass(), qualifier);
                else
                    bot = App.ctx.getBean(bot.getClass());
                bot.start();
            }
        }, 10, 10, TimeUnit.SECONDS);
    }
}
