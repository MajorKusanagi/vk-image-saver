package app;

import app.spring.config.YamlConfig;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class LogController {
    private static Logger nsfwLog = LogManager.getLogger("nsfw");
    private static Logger memeLog = LogManager.getLogger("meme");
    private static YamlConfig appConfig = App.ctx.getBean(YamlConfig.class);

    public static void log(int groupId, Level logLevel, String info) {
        final int nsfwGroupId = appConfig.getNsfwBot().getGroupId();
        final int memeGroupId = appConfig.getMemeBot().getGroupId();

        if(groupId == nsfwGroupId) {
            nsfwLog.log(logLevel, info);
            return;
        }

        if(groupId == memeGroupId) {
            memeLog.log(logLevel, info);
            return;
        }
    }
}
