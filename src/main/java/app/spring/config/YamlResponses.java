package app.spring.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;

import java.util.List;

@ConstructorBinding
@ConfigurationProperties
public class YamlResponses {
    private BotResponses nsfwResponses;
    private BotResponses memeResponses;

    public YamlResponses(BotResponses nsfwResponses, BotResponses memeResponses) {
        this.nsfwResponses = nsfwResponses;
        this.memeResponses = memeResponses;
    }

    public BotResponses getNsfwResponses() {
        return nsfwResponses;
    }

    public BotResponses getMemeResponses() {
        return memeResponses;
    }

    public static class BotResponses {
        List<String> catched;
        List<String> pong;
        List<String> noImage;
        List<String> error;

        public BotResponses(List<String> catched, List<String> pong, List<String> noImage, List<String> error) {
            this.catched = catched;
            this.pong = pong;
            this.noImage = noImage;
            this.error = error;
        }

        public List<String> getCatched() {
            return catched;
        }

        public List<String> getPong() {
            return pong;
        }

        public List<String> getNoImage() {
            return noImage;
        }

        public List<String> getError() {
            return error;
        }
    }
}
