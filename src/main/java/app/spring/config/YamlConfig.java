package app.spring.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;

@ConstructorBinding
@ConfigurationProperties
public class YamlConfig {
    private BotSettings nsfwBot;
    private BotSettings memeBot;

    public YamlConfig(BotSettings nsfwBot, BotSettings memeBot) {
        this.nsfwBot = nsfwBot;
        this.memeBot = memeBot;
    }

    public BotSettings getNsfwBot() {
        return nsfwBot;
    }

    public BotSettings getMemeBot() {
        return memeBot;
    }

    public static class BotSettings {
        private int groupId;
        private String accessToken;
        private String savePath;

        public BotSettings(int groupId, String accessToken, String savePath) {
            this.groupId = groupId;
            this.accessToken = accessToken;
            this.savePath = savePath;
        }

        public int getGroupId() {
            return groupId;
        }

        public String getAccessToken() {
            return accessToken;
        }

        public String getSavePath() {
            return savePath;
        }
    }
}