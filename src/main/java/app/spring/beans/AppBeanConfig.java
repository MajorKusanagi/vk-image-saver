package app.spring.beans;

import app.spring.config.YamlConfig;
import app.spring.config.YamlLoader;
import app.spring.config.YamlResponses;
import com.vk.api.sdk.client.VkApiClient;
import com.vk.api.sdk.httpclient.HttpTransportClient;
import org.springframework.boot.autoconfigure.AutoConfigurationPackage;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.*;

@AutoConfigurationPackage
@Configuration
@PropertySource(value = {"classpath:appConfig.yml", "classpath:botResponses.yml"}, factory = YamlLoader.class)
@EnableConfigurationProperties({YamlConfig.class, YamlResponses.class})
public class AppBeanConfig {
    @Bean
    @Scope("singleton")
    public VkApiClient vkClient() {
        return new VkApiClient(HttpTransportClient.getInstance());
    }
}
