package app.spring.beans;

import app.spring.config.YamlConfig;
import app.spring.config.YamlResponses;
import bot.Launcher;
import bot.vk.*;
import com.vk.api.sdk.client.VkApiClient;
import com.vk.api.sdk.client.actors.GroupActor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class VkNsfwBeanConfig
{
    @Bean(name = "nsfwChan")
    @Qualifier("nsfw")
    @Scope("singleton")
    public Launcher nsfwBotLauncher(@Qualifier("nsfw") VkBot bot) {
        return new Launcher(bot, "nsfw");
    }

    @Bean
    @Qualifier("nsfw")
    @Scope("prototype")
    public VkBot nsfwBotThread(@Qualifier("nsfw") GroupActor groupActor) {
        return new VkBot(groupActor, "nsfw");
    }

    @Bean
    @Qualifier("nsfw")
    @Scope("singleton")
    public GroupActor nsfwGroupActor(YamlConfig appConfig) {
        int groupId = appConfig.getNsfwBot().getGroupId();
        String accessToken = appConfig.getNsfwBot().getAccessToken();
        return new GroupActor(groupId, accessToken);
    }

    @Bean
    @Qualifier("nsfw")
    @Scope("singleton")
    public ApiEventHandler nsfwEventHandler(VkApiClient vkClient, @Qualifier("nsfw") GroupActor groupActor, @Qualifier("nsfw") MessageHandler messageHandler) {
        return new ApiEventHandler(vkClient, groupActor, messageHandler);
    }

    @Bean
    @Qualifier("nsfw")
    @Scope("singleton")
    public MessageHandler nsfwMessageHandler(@Qualifier("nsfw") ImageSaver imageSaver, @Qualifier("nsfw") ResponseMaker responseMaker) {
        return new MessageHandler(imageSaver, responseMaker);
    }

    @Bean
    @Qualifier("nsfw")
    @Scope("singleton")
    public ResponseMaker nsfwResponseMaker(@Qualifier("nsfw") GroupActor groupActor, YamlResponses yamlResponses) {
        YamlResponses.BotResponses responses = yamlResponses.getNsfwResponses();
        return new ResponseMaker(groupActor, responses);
    }

    @Bean
    @Qualifier("nsfw")
    @Scope("singleton")
    public ImageSaver nsfwImageSaver(YamlConfig appConfig) {
        String savePath = appConfig.getNsfwBot().getSavePath();
        return new ImageSaver(savePath);
    }
}