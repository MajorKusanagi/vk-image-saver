package app.spring.beans;

import app.spring.config.YamlConfig;
import app.spring.config.YamlResponses;
import bot.Launcher;
import bot.vk.*;
import com.vk.api.sdk.client.VkApiClient;
import com.vk.api.sdk.client.actors.GroupActor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class VkMemeBeanConfig {
    @Bean(name = "memeChan")
    @Qualifier("meme")
    @Scope("singleton")
    public Launcher memeBotLauncher(@Qualifier("meme") VkBot bot) {
        return new Launcher(bot, "meme");
    }

    @Bean
    @Qualifier("meme")
    @Scope("prototype")
    public VkBot memeBotThread(@Qualifier("meme") GroupActor groupActor) {
        return new VkBot(groupActor, "meme");
    }

    @Bean
    @Qualifier("meme")
    @Scope("singleton")
    public GroupActor memeGroupActor(YamlConfig appConfig) {
        int groupId = appConfig.getMemeBot().getGroupId();
        String accessToken = appConfig.getMemeBot().getAccessToken();
        return new GroupActor(groupId, accessToken);
    }

    @Bean
    @Qualifier("meme")
    @Scope("singleton")
    public ApiEventHandler memeEventHandler(VkApiClient vkClient, @Qualifier("meme") GroupActor groupActor, @Qualifier("meme") MessageHandler messageHandler) {
        return new ApiEventHandler(vkClient, groupActor, messageHandler);
    }

    @Bean
    @Qualifier("meme")
    @Scope("singleton")
    public MessageHandler memeMessageHandler(@Qualifier("meme") ImageSaver imageSaver, @Qualifier("meme") ResponseMaker responseMaker) {
        return new MessageHandler(imageSaver, responseMaker);
    }

    @Bean
    @Qualifier("meme")
    @Scope("singleton")
    public ResponseMaker memeResponseMaker(@Qualifier("meme") GroupActor groupActor, YamlResponses yamlResponses) {
        YamlResponses.BotResponses responses = yamlResponses.getMemeResponses();
        return new ResponseMaker(groupActor, responses);
    }

    @Bean
    @Qualifier("meme")
    @Scope("singleton")
    public ImageSaver memeImageSaver(YamlConfig appConfig) {
        String savePath = appConfig.getMemeBot().getSavePath();
        return new ImageSaver(savePath);
    }
}