package app;

import bot.Launcher;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Import;
import app.spring.beans.AppBeanConfig;
import app.spring.beans.VkMemeBeanConfig;
import app.spring.beans.VkNsfwBeanConfig;

@Import({AppBeanConfig.class, VkNsfwBeanConfig.class, VkMemeBeanConfig.class})
public class App {
    public static ApplicationContext ctx;

    public static void main(String[] args) {
        ctx = SpringApplication.run(App.class, args);

        App.ctx.getBean("nsfwChan", Launcher.class).launch();
        App.ctx.getBean("memeChan", Launcher.class).launch();
    }
}
